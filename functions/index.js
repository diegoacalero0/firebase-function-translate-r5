const functions = require("firebase-functions");
const translatte = require("translatte");

exports.myFunction = functions.firestore
    .document("users/{userDocId}/tasks/{taskId}")
    .onWrite((change, context) => {
      const data = change.after.data();
      return translatte(data.title, {to: "en", from: "es"}).then((res) => {
        const translatedTitle = res.text;
        return translatte(data.description, {
          to: "en",
          from: "es",
        }).then((res) => {
          const translatedDescription = res.text;
          return change.after.ref.set({
            translatedTitle: translatedTitle,
            translatedDescription: translatedDescription,
          }, {merge: true});
        });
      });
    }
    );
